<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wsb
 */

?>
	</section>

	<footer id="colophon" class="site-footer">
		<section class="top">
			<div class="container">
				<div class="col-lg-8 col-md-8 col-sm-12">
					<div class="row">
						<div class="col-lg-2 col-md-2">
							<div class="branding">
								<?php
								the_custom_logo();
								?>
							</div>
							<?php if( get_field('selo', 'option') ): ?>
								<img src="<?php the_field('selo', 'option'); ?>">
							<?php endif; ?>
							
						</div>
						<div class="col-lg-2 col-md-2">
							<?php if( get_field('ttl_naveg', 'option') ): ?>
								<h3><?php the_field('ttl_naveg', 'option'); ?></h3>
							<?php endif; ?>
							<?php
							wp_nav_menu( array(
								'theme_location' => 'menu-1'
							) );
							?>
							
						</div>
						<div class="col-lg-3 col-md-3">
							<?php if( get_field('ttl_contato', 'option') ): ?>
								<h3><?php the_field('ttl_contato', 'option'); ?></h3>
							<?php endif; ?>
				    		<?php if ( have_rows('contato', 'option') ): ?>
				    			<ul class="contato">
				                <?php while ( have_rows('contato', 'option') ) : the_row(); ?>
				                    <li>

										<?php if( get_sub_field('link', 'option') ): ?>
				                        	<a href="<?php the_sub_field('link'); ?>" target="_blank">
										<?php endif; ?>
											<img src="<?php the_sub_field('icone'); ?>">
				                        	<span><?php the_sub_field('texto'); ?></span>
										<?php if( get_sub_field('link', 'option') ): ?>
				                        	</a>
										<?php endif; ?>
				                    </li>
				                <?php endwhile; ?>
				    			</ul>
				    		<?php endif; ?>	


							
						</div>
						<div class="col-lg-4 col-md-4">
							<?php if( get_field('ttl_social', 'option') ): ?>
								<h3><?php the_field('ttl_social', 'option'); ?></h3>
							<?php endif; ?>
				    		<?php if ( have_rows('redes_sociais', 'option') ): ?>
				    			<ul class="redes_sociais">
				                <?php while ( have_rows('redes_sociais', 'option') ) : the_row(); 
				                	$pos = get_sub_field('pos');
									if( $pos && in_array('footer', $pos) ) { ?>
				                    <li>
				                        <a href="<?php the_sub_field('link'); ?>" target="_blank">
				                        	<img src="<?php the_sub_field('icone'); ?>">
				                        	<span><?php the_sub_field('nome'); ?></span>
				                        </a>
				                    </li>
				                <?php } endwhile; ?>
				    			</ul>
				    		<?php endif; ?>	
							
						</div>
					</div>
					<div class="row mtop">
						<div class="col-lg-8 copy col-md-9">
							<?php if( get_field('copyrights', 'option') ): ?>
								<?php the_field('copyrights', 'option'); ?>
							<?php endif; ?>
						</div>
						<div class="col-lg-4  col-md-3">
							<?php if( get_field('sign', 'option') ): ?>
								<a href="<?php the_field('link_sign', 'option'); ?>" target="_blank" class="sign" title="Feito com qualitare">
									<img src="<?php the_field('sign', 'option'); ?>" alt="Feito com qualitare">
								</a>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12">
					<div class="blue-bg">
						
						<?php if( get_field('ttl_news', 'option') ): ?>
							<h2><?php the_field('ttl_news', 'option'); ?></h2>
						<?php endif; ?>

						<?php the_field('short', 'option'); ?>
					</div>
				</div>
			</div>
		</section>
	</footer><!-- #colophon -->
</div><!-- #page -->



<?php if ( have_rows('botao') ): ?>
    <?php while ( have_rows('botao') ) : 
    	the_row(); ?>
		<?php $estilo_botao = get_sub_field('estilo_botao');
		if( $estilo_botao && in_array('video', $estilo_botao) ) : ?>
        <div class="video-popup" id="vid1">
        	<div class="pop-cont">
        		<button class="close"></button>
				<div class="videoWrapper">
		        	<?php the_sub_field('video'); ?>
		        </div>
		    </div>
        </div>
        <?php endif;
    endwhile;
endif; ?>



<?php if ( have_rows('botao_2') ): ?>
    <?php while ( have_rows('botao_2') ) : 
    	the_row(); ?>
		<?php $estilo_botao = get_sub_field('estilo_botao');
		if( $estilo_botao && in_array('video', $estilo_botao) ) : ?>
        <div class="video-popup" id="vid2">
        	<div class="pop-cont">
        		<button class="close"></button>
				<div class="videoWrapper">
		        	<?php the_sub_field('video'); ?>
		        </div>
		    </div>
        </div>
        <?php endif;
    endwhile;
endif; ?>



<?php if ( have_rows('botao_3') ): ?>
    <?php while ( have_rows('botao_3') ) : 
    	the_row(); ?>
		<?php $estilo_botao = get_sub_field('estilo_botao');
		if( $estilo_botao && in_array('video', $estilo_botao) ) : ?>
        <div class="video-popup" id="vid3">
        	<div class="pop-cont">
        		<button class="close"></button>
				<div class="videoWrapper">
		        	<?php the_sub_field('video'); ?>
		        </div>
		    </div>
        </div>
        <?php endif;
    endwhile;
endif; ?>

<?php wp_footer(); ?>

<script type="text/javascript">
	

jQuery(function(){
    var hasBeenTrigged = false;
    jQuery(window).scroll(function() {
        if (jQuery(this).scrollTop() >= 50 && !hasBeenTrigged) { // if scroll is greater/equal then 100 and hasBeenTrigged is set to false.
             jQuery('header').addClass('scroll');
            hasBeenTrigged = true;
        } else if(jQuery(this).scrollTop() < 50 && hasBeenTrigged){
             jQuery('header').removeClass('scroll');
            hasBeenTrigged = false;

        }
    });
});

jQuery(document).delegate('.btn_large.video', 'click', function(event) {
    event.preventDefault();
    var link = jQuery(this).attr('href');
   jQuery('.video-popup' + link).addClass('active');
    setTimeout( function() {
   		jQuery('.video-popup' + link).addClass('show');
    
    }, 200); 
});

jQuery(document).mouseup(function(e) 
{

	if (jQuery( ".video-popup" ).hasClass( "active" )) {
    var container = jQuery(".video-popup.active .videoWrapper");
	    if (!container.is(e.target) && container.has(e.target).length === 0) 
	    {
	    	jQuery('.video-popup').removeClass('show');
		    setTimeout( function() {
	    		jQuery('.video-popup').removeClass('active');
		    	jQuery('.video-popup iframe').each(function(index) {
			        jQuery(this).attr('src', jQuery(this).attr('src'));
			        return false;
			    });
		    }, 600); 
	    }
	}
});



</script>
</body>
</html>
