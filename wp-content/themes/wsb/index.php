<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wsb
 */

get_header();
?>


<section class="title-top conteudo-sec ttl-search-form">
	<div class="container">
		<div class="col-lg-8 col-md-7 col-xs-12">
			<?php if( get_field('titulo_cont', 'option') ): ?>
				<h1>
					<?php the_field('titulo_cont', 'option'); ?>
				</h1>
			<?php endif; ?>
		</div>
		<div class="col-lg-4 col-md-4 col-xs-12">
			<?php echo get_search_form();?>
		</div>
	</div>
</section>

<?php $args = array(
        'post_type' => 'post',
        'orderby'=>'rand',
        'posts_per_page' => 5,
        'tax_query' => array(
            array(
                'taxonomy' => 'destaque',
                'field' => 'name',
                'terms' => 'Conteúdo',
            ),
        ),
    );

$query = new WP_Query( $args );
if( $query->have_posts() ){    ?>
<section class="destaque conteudo-sec">
	<div class="container">
		<div class="col-xs-12 responsabilidade">
			<?php if( get_field('destaques', 'option') ): ?>
				<h2>
					<span><?php the_field('destaques', 'option'); ?></span>
				</h2>
			<?php endif; 
         
            echo '<ul class="conteudos">';
            while( $query->have_posts() ){
                $query->the_post();
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
                echo '<li>
                        <ul class="blog-categories">';

                        $categories = wp_get_post_categories( get_the_ID() );
                            //loop through them
                            foreach($categories as $c){
                              $cat = get_category( $c );
                              //get the name of the category
                              $cat_id = get_cat_ID( $cat->name );
                              //make a list item containing a link to the category
                               echo '<li><a href="'.get_category_link($cat_id).'" title="'.$cat->name.'">'.$cat->name.'</a></li>';
                            }
                        echo '
                        </ul> 
                        <a href="'.get_the_permalink().'" title="'.get_the_title().'" class="img" style="background: linear-gradient(0deg, #2A437A, #2A437A), linear-gradient(0deg, rgba(65, 87, 137, 0.4), rgba(65, 87, 137, 0.4)), url('.$image[0].'); background-blend-mode: color, multiply, normal;
                        ">

                        </a>
                        <a href="'.get_the_permalink().'" title="'.get_the_title().'" class="info">
                            <span>'.get_the_date().'</span>
                            <h3>'.get_the_title().'</h3>
                        </a>                 
                    </li>';
            }

            echo '</ul>';
            ?>
		</div>
	</div>
</section>
<?php } ?>

<section class=" conteudo-sec mid">
	<div class="container">
		<div class="col-lg-8 col-md-8">
			<?php if( get_field('recentes', 'option') ): ?>
				<h2>
					<?php the_field('recentes', 'option'); ?>
				</h2>
			<?php endif; ?>

	 	<?php echo do_shortcode( '[ajax_load_more id="8900039637" loading_style="blue" container_type="ul" css_classes="conteudos" post_type="post" button_label="Load More" archive="true" scroll="false"]' ); ?>


		</div>
		<div class="col-lg-4 col-md-4">
			<?php get_sidebar(); ?>
		</div>

	</div>
	
</section>

<?php
get_footer();