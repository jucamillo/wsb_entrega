<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wsb
 */


if ( !is_front_page() ) :
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ) ;
echo '<section class="top" style="background-image:url('.$image[0].');"><section class="bottom"><div class="container"><h1>'.get_the_title().'</h1></div></section></section>';
	endif; ?>


<section id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php
		the_content();
		?>

</section><!-- #post-<?php the_ID(); ?> -->
