<?php /* Template Name: Nosso Jeito */ ?>
<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wsb
 */

get_header();
?>

		<?php
		while ( have_posts() ) :
			the_post(); ?>
				<section class="vc_section" id="jeito1" style="background-image: url(<?php the_field('bg'); ?>);">
					<div class="container" >
							
						<div class="col-md-6 col-sm-8  rellax" data-rellax-speed="-5">
				                	<div class="full" id="pin1">
							<?php if( get_field('titulo') ): ?>
								<h1>
									<?php the_field('titulo'); ?>
								</h1>
							<?php endif; ?>

							<?php if( get_field('texto') ): ?>
								<div class="txt">
									
								<?php the_field('texto'); ?>
								</div>
							<?php endif; ?>
				    		<?php if ( have_rows('botao') ): ?>
				                <?php while ( have_rows('botao') ) : the_row(); ?>
				                	<?php $estilo_botao = get_sub_field('estilo_botao');

		                			//VIDEO BOTAO
									if( $estilo_botao && in_array('video', $estilo_botao) ) : ?>

			                        <a href="#vid1" class="btn_large video">
			                        	<span>
			                        		<?php the_sub_field('texto'); ?>	                        		
			                        	</span>
										<?php if( get_sub_field('icone') ): ?>
				                        <img src="<?php the_sub_field('icone'); ?>">
										<?php else: ?>
										<img src="<?php echo get_template_directory_uri(); ?>/images/play.svg">
										<?php endif; ?>
			                        </a>

									<?php 
		                			//DOWNLOAD BOTAO
									elseif ( $estilo_botao && in_array('download', $estilo_botao)) : ?>

										<?php if( get_sub_field('arquivo') ): ?>
										<a href="<?php the_sub_field('arquivo'); ?>" target="<?php the_sub_field('target'); ?>" class="btn_large" >
										<?php else: ?>
										<a href="<?php the_sub_field('link'); ?>" target="<?php the_sub_field('target'); ?>" class="btn_large" >
										<?php endif; ?>

			                        	<span>
			                        		<?php the_sub_field('texto'); ?>	                        		
			                        	</span>

										<?php if( get_sub_field('icone') ): ?>
				                        <img src="<?php the_sub_field('icone'); ?>">
										<?php else: ?>
										<img src="<?php echo get_template_directory_uri(); ?>/images/ico-bt-down.svg">
										<?php endif; ?>
			                        </a>

									<?php 
		                			//Botão padrão
		                			else: ?>
			                        <a href="<?php the_sub_field('link'); ?>" target="<?php the_sub_field('target'); ?>" class="btn_large" >
			                        	<span>
			                        		<?php the_sub_field('texto'); ?>	                        		
			                        	</span>
										<?php if( get_sub_field('icone') ): ?>
				                        <img src="<?php the_sub_field('icone'); ?>">
										<?php endif; ?>
			                        </a>
				                    <?php endif; ?>
				                <?php endwhile; ?>
								</div>
				    		<?php endif; ?>	
						</div>
						<div class="col-md-6 col-sm-4">
							<div class="sq  rellax" data-rellax-speed="2"></div>
						</div>
					</div>
				</section>

				<section class="vc_section" id="jeito2">
					<div class="container">
						<div class="col-lg-6 col-md-6 col-sm-12">
							<?php if( get_field('titulo_2') ): ?>
								<h1 class="">
									<?php the_field('titulo_2'); ?>
								</h1>
							<?php endif; ?>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12 rellax" data-rellax-speed="-1">
							<?php if( get_field('texto_2') ): ?>
								<div class="txt">
									<?php the_field('texto_2'); ?>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</section>

				<section class="vc_section" id="jeito3">
		    		<?php if ( have_rows('tec') ): ?>
		    			<ul class="tec">
		                <?php while ( have_rows('tec') ) : the_row();  ?>
		                    <li style="left:calc(<?php the_sub_field('left'); ?>% + 15px); top: <?php the_sub_field('top'); ?>%;">

								<?php if( get_sub_field('desc', 'option') ): ?>
		                        	<a href="#" class="pop-desc">
								<?php endif; ?>
										<span><?php the_sub_field('nome'); ?></span>
								<?php if( get_sub_field('desc', 'option') ): ?>
		 								<img src="<?php echo get_template_directory_uri(); ?>/images/ico.svg" class="ico">
		                        	</a>
		                        	<div class="pop">
		                        		<?php the_sub_field('desc'); ?>
										<?php if( get_sub_field('link_saiba_mais', 'option') ): ?>
				                        	<a href="<?php the_sub_field('link_saiba_mais'); ?>" target="_blank" class="more">
				                        		Saiba mais
				                        	</a>
										<?php endif; ?>
		                        	</div>
								<?php endif; ?>
		                    </li>
		                <?php endwhile; ?>
		    			</ul>
		    		<?php endif; ?>	
		 			<img src="<?php echo get_template_directory_uri(); ?>/images/maptec.svg" class="bg desktop">
		 			<img src="<?php echo get_template_directory_uri(); ?>/images/maptec2.svg" class="bg mobile">
				</section>


					
				<section class="vc_section" id="jeito4">
					<div class="container">
						<div class="col-lg-4 col-md-2 col-sm-12"> 
							
						</div>
						<div class="col-lg-4 col-md-4 col-sm-12">
							<?php if( get_field('titulo_3') ): ?>
									<h1>
										<?php the_field('titulo_3'); ?>
									</h1>
							<?php endif; ?>
				    		<?php if ( have_rows('botao_3') ): ?>
				                <?php while ( have_rows('botao_3') ) : the_row(); ?>


				                	<?php $estilo_botao = get_sub_field('estilo_botao');

		                			//VIDEO BOTAO
									if( $estilo_botao && in_array('video', $estilo_botao) ) : ?>

			                        <a href="#vid3" class="btn_large video">
			                        	<span>
			                        		<?php the_sub_field('texto'); ?>	                        		
			                        	</span>
										<?php if( get_sub_field('icone') ): ?>
				                        <img src="<?php the_sub_field('icone'); ?>">
										<?php else: ?>
										<img src="<?php echo get_template_directory_uri(); ?>/images/play.svg">
										<?php endif; ?>
			                        </a>

									<?php 
		                			//DOWNLOAD BOTAO
									elseif ( $estilo_botao && in_array('download', $estilo_botao)) : ?>

										<?php if( get_sub_field('arquivo') ): ?>
										<a href="<?php the_sub_field('arquivo'); ?>" target="<?php the_sub_field('target'); ?>" class="btn_large" >
										<?php else: ?>
										<a href="<?php the_sub_field('link'); ?>" target="<?php the_sub_field('target'); ?>" class="btn_large" >
										<?php endif; ?>

			                        	<span>
			                        		<?php the_sub_field('texto'); ?>	                        		
			                        	</span>

										<?php if( get_sub_field('icone') ): ?>
				                        <img src="<?php the_sub_field('icone'); ?>">
										<?php else: ?>
										<img src="<?php echo get_template_directory_uri(); ?>/images/ico-bt-down.svg">
										<?php endif; ?>
			                        </a>

									<?php 
		                			//Botão padrão
		                			else: ?>
			                        <a href="<?php the_sub_field('link'); ?>" target="<?php the_sub_field('target'); ?>" class="btn_large" >
			                        	<span>
			                        		<?php the_sub_field('texto'); ?>	                        		
			                        	</span>
										<?php if( get_sub_field('icone') ): ?>
				                        <img src="<?php the_sub_field('icone'); ?>">
										<?php endif; ?>
			                        </a>
				                    <?php endif; ?>
				                <?php endwhile; ?>
				    		<?php endif; ?>	
						</div>
						<div class="col-lg-4 col-md-5 col-sm-12 rellax" data-rellax-speed="3">
				    		<?php if ( have_rows('valores') ): ?>
				    			<ul class="valores">
				                <?php while ( have_rows('valores') ) : the_row();  ?>
				                    <li>
				                    	<?php the_sub_field('nome'); ?>
				                    </li>
				                <?php endwhile; ?>
				    			</ul>
				    		<?php endif; ?>	
						</div>
					</div>
				</section>


				<section class="vc_section" id="jeito5" style="background-image: url(<?php the_field('bg_4'); ?>);">
					<div class="container">
						<div class="col-lg-5  col-md-6 col-sm-12">
							<div id="pinj5" class="full rellax" data-rellax-speed="-3">
								
							<?php if( get_field('titulo_4') ): ?>
								<h1>
									<?php the_field('titulo_4'); ?>
								</h1>
							<?php endif; ?>
							<?php if( get_field('texto_4') ): ?>
								<div class="txt">
									<?php the_field('texto_4'); ?>
								</div>
							<?php endif; ?>
							</div>
						</div>
						<div class="col-lg-1 col-md-1 display-md-absolute"><div class="spacerj5 full"></div><div class="full triggerj5"></div></div>
						<div class="col-lg-6  col-md-6 col-sm-12 rellax" data-rellax-speed="-8">
							<?php 
								$term = get_field('categorias_conteudo');
								if( $term ): ?>
									<div class="responsabilidade r1">
	 									<?php echo do_shortcode( '[responsabilidade categoria="'.esc_html( $term->name ).'"]' ); ?>
				                        <a href="<?php echo esc_url( get_term_link( $term ) ); ?>" class="btn_large" >
				                            <span>
				                            Veja <br>
				                            Mais                                 
				                            </span>
				                        </a>
									</div>
								<?php endif; ?>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12  rellax" data-rellax-speed="3">
							<?php 
								$term = get_field('categorias_conteudo2');
								if( $term ): ?>
									<div class="responsabilidade">
	 									<?php echo do_shortcode( '[responsabilidade categoria="'.esc_html( $term->name ).'"]' ); ?>
				                        <a href="<?php echo esc_url( get_term_link( $term ) ); ?>" class="btn_large" >
				                            <span>
				                            Veja <br>
				                            Mais                                 
				                            </span>
				                        </a>
									</div>
								<?php endif; ?>
						</div>
					</div>
				</section>
		<?php
		endwhile; // End of the loop.
		?>

<script type="text/javascript">
/*
var controller = new ScrollMagic.Controller();

var scenePin2 = new ScrollMagic.Scene({
	duration:(jQuery(".responsabilidade.r1").height() - jQuery("div#pinj5").height()),
	triggerElement:".triggerj5"
 })
.setPin("#pinj5")
//.addIndicators({name: "2 "}) // add indicators (requires plugin)
.addTo(controller);


/*

var sceneBT1 = new ScrollMagic.Scene({
 duration:100,
 offset:0
})
.setPin("#pin1")
.addTo(controller);


*/



jQuery(document).delegate('#jeito3 ul li a.pop-desc', 'click', function(event) {
    event.preventDefault();
});

var rellax = new Rellax('.rellax', {
        center: true
      });





        if (jQuery(window).width() < 992) {


            jQuery('#jeito4 .btn_large').insertAfter('#jeito4 ul.valores');
         
            
        }  


</script>
<?php
get_footer();
