<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package wsb
 */

get_header();
?>
<section class="vc_section single-area-section">
	<?php while ( have_posts() ) : the_post();
	                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );  ?>
		<div class="container">
			<div class="col-xs-12">
				<div class="bg-top" style="background-image: url(<?php echo $image[0]; ?>);">
					
					<h1><?php the_title(); ?></h1>
				</div>
				
			</div>
			
			<div class="col-lg-8 col-md-8">
				<div class="resumo">
	            	<?php the_excerpt(); ?>
				</div>
			</div>
			<div class="col-lg-4 col-md-4">
				<div class="bg-blue">
				</div>
			</div>
			<div class="col-lg-8 col-md-8">
				<div class="info">
	            	<?php the_content( ); ?>
					
				    		<?php if ( have_rows('botao') ): ?>
				    			<ul class="bts">
				                <?php while ( have_rows('botao') ) : the_row(); ?>
				                	<li>

				                    
					                    <?php $estilo_botao = get_sub_field('estilo_botao');

			                			//DOWNLOAD BOTAO
										if ( $estilo_botao && in_array('pdf', $estilo_botao) ) : ?>

											<?php if( get_sub_field('arquivo') ): ?>
											<a href="<?php the_sub_field('arquivo'); ?>" target="<?php the_sub_field('target'); ?>" class="btn_large" >
											<?php else: ?>
											<a href="<?php the_sub_field('link'); ?>" target="<?php the_sub_field('target'); ?>" class="btn_large" >
											<?php endif; ?>

				                        	<span>
				                        		<?php the_sub_field('texto'); ?>	                        		
				                        	</span>

											<?php if( get_sub_field('icone') ): ?>
					                        <img src="<?php the_sub_field('icone'); ?>">
											<?php else: ?>
											<img src="<?php echo get_template_directory_uri(); ?>/images/pdf.svg">
											<?php endif; ?>
				                        </a>


				                        <?php 
				                        elseif ( $estilo_botao && in_array('publicacoes', $estilo_botao) ) : ?>

											<?php if( get_sub_field('arquivo') ): ?>
											<a href="<?php the_sub_field('arquivo'); ?>" target="<?php the_sub_field('target'); ?>" class="btn_large" >
											<?php else: ?>
											<a href="<?php the_sub_field('link'); ?>" target="<?php the_sub_field('target'); ?>" class="btn_large" >
											<?php endif; ?>

				                        	<span>
				                        		<?php the_sub_field('texto'); ?>	                        		
				                        	</span>

											<?php if( get_sub_field('icone') ): ?>
					                        <img src="<?php the_sub_field('icone'); ?>">
											<?php else: ?>
											<img src="<?php echo get_template_directory_uri(); ?>/images/publicacoes.svg">
											<?php endif; ?>
				                        </a>



				                        <?php 
				                        elseif ( $estilo_botao && in_array('noticias', $estilo_botao) ) : ?>

											<?php if( get_sub_field('arquivo') ): ?>
											<a href="<?php the_sub_field('arquivo'); ?>" target="<?php the_sub_field('target'); ?>" class="btn_large" >
											<?php else: ?>
											<a href="<?php the_sub_field('link'); ?>" target="<?php the_sub_field('target'); ?>" class="btn_large" >
											<?php endif; ?>

				                        	<span>
				                        		<?php the_sub_field('texto'); ?>	                        		
				                        	</span>

											<?php if( get_sub_field('icone') ): ?>
					                        <img src="<?php the_sub_field('icone'); ?>">
											<?php else: ?>
											<img src="<?php echo get_template_directory_uri(); ?>/images/noticias.svg">
											<?php endif; ?>
				                        </a>



				                        <?php 
				                        elseif ( $estilo_botao && in_array('download', $estilo_botao) ) : ?>

											<?php if( get_sub_field('arquivo') ): ?>
											<a href="<?php the_sub_field('arquivo'); ?>" target="<?php the_sub_field('target'); ?>" class="btn_large" >
											<?php else: ?>
											<a href="<?php the_sub_field('link'); ?>" target="<?php the_sub_field('target'); ?>" class="btn_large" >
											<?php endif; ?>

				                        	<span>
				                        		<?php the_sub_field('texto'); ?>	                        		
				                        	</span>

											<?php if( get_sub_field('icone') ): ?>
					                        <img src="<?php the_sub_field('icone'); ?>">
											<?php else: ?>
											<img src="<?php echo get_template_directory_uri(); ?>/images/ico-bt-down.svg">
											<?php endif; ?>
				                        </a>

										<?php 
			                			//Botão padrão
			                			else: ?>
				                        <a href="<?php the_sub_field('link'); ?>" target="<?php the_sub_field('target'); ?>" class="btn_large" >
				                        	<span>
				                        		<?php the_sub_field('texto'); ?>	                        		
				                        	</span>
											<?php if( get_sub_field('icone') ): ?>
					                        <img src="<?php the_sub_field('icone'); ?>">
											<?php endif; ?>


				                        </a>
					                    <?php endif; ?>

				                	</li>
				                <?php endwhile; ?>
				                </ul>
				    		<?php endif; ?>	


				    		<div class="share">
								<?php if( get_field('chamada', 'option') ): ?>
									<h3>
										<span><?php the_field('chamada', 'option'); ?></span>
									</h3>
								<?php endif; ?>
									<?php if(is_active_sidebar('share')){ dynamic_sidebar('share'); } ?>
				    		</div>

				    		<div class="back-mobile">
				    			<a href="javascript:history.back()" class="btnmd">
				    				Voltar
				    			</a>
				    		</div>

				</div>
			</div>

			<div class="col-lg-4 col-md-4">
				<div class="bg-blue">
					<?php
					$bannerArgs = array( 
					        'post_type' => 'area', 
					        'posts_per_page' => -1, 
					        'orderby'=>'name',
					        'order'=>'asc'
					    );
					    $bannerLoop = new WP_Query( $bannerArgs ); 
					    echo '<ul class="list-areas">';
					    while ( $bannerLoop->have_posts() ) : $bannerLoop->the_post();
					    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
					    echo '<li> <a href="'.get_the_permalink().'" title="'.get_the_title().'">'.get_the_title().'</a></li>';
					    endwhile;
					    echo '</ul>';

					 ?>
				</div>
			</div>
		</div>





	<?php endwhile; ?>
</section>

<?php
get_footer();
