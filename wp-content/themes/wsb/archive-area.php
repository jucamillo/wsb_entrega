<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package wsb
 */

get_header();
?>

<section class="vc_section">
	<div class="container">
		<div class="col-md-6 rellax" data-rellax-speed="-5">
			<?php if( get_field('ttl_areas', 'option') ): ?>
				<div class="full" id="pin">
					
				<h1><?php the_field('ttl_areas', 'option'); ?></h1>
			<?php endif; ?>
				</div>
			
		</div>
		<div class="col-md-6">
			<?php if ( have_posts() ) :?>

			<ul class="areas">
			<?php while ( have_posts() ) : the_post();
	                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );  ?>
				<li>
					<a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>" data-bg="<?php echo $image[0]; ?>">
						<?php the_title(); ?>
					</a>
				</li>
			<?php endwhile; ?>
			</ul>

			<?php   wpbeginner_numeric_posts_nav(); 
			else : ?>
			<h2>
				Nenhuma área de atuação encontrada
			</h2>
			<?php endif;	?>
		</div>

	</div>
</section>

<script type="text/javascript">

	/**/
var controller = new ScrollMagic.Controller();





var sceneBT1 = new ScrollMagic.Scene({
 duration: (jQuery("ul.areas").height() - jQuery("#pin").height() ),
 offset:0
})
.setPin("#pin")
.addTo(controller);



jQuery("ul.areas li a").hover(function(){
    var bg = jQuery(this).attr('data-bg');
    console.log(bg);
    jQuery('.vc_section').css('background-image', 'url('+bg+')');
});
    
jQuery(function(){
	var bg1 = jQuery("ul.areas li:nth-child(1) a").attr('data-bg');
    jQuery('.vc_section').css('background-image', 'url('+bg1+')');
});


</script>
<?php
//get_sidebar();
get_footer();
