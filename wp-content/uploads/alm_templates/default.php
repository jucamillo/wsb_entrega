<?php
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
	                echo '<li>
			                <div class="sqimg">
	                        <a href="'.get_the_permalink().'" title="'.get_the_title().'" class="img" style="background-image: url('.$image[0].');">
	                        </a>
	                        </div>
	                        <div class="info">

		                        <ul class="blog-categories">';

		                        $categories = wp_get_post_categories( get_the_ID() );
		                            //loop through them
		                            foreach($categories as $c){
		                              $cat = get_category( $c );
		                              //get the name of the category
		                              $cat_id = get_cat_ID( $cat->name );
		                              //make a list item containing a link to the category
		                               echo '<li><a href="'.get_category_link($cat_id).'" title="'.$cat->name.'">'.$cat->name.'</a></li>';
		                            }
		                        echo '
		                        </ul> 
	                            <span>'.get_the_date().'</span>
	                            <h3><a href="'.get_the_permalink().'" title="'.get_the_title().'"> '.get_the_title().'</a></h3>
						        <p>
						            '.strip_tags( get_the_excerpt() ).'
						        </p>
						        <a href="'.get_the_permalink().'" class="btn" title="'.__( 'Keep reading', 'wsb' ).'">
						        	'.__( 'Keep reading', 'wsb' ).'
						        </a>
                        </div>                 
	                    </li>'; ?>